#!/bin/bash

set -xeuo pipefail
IFS=$'\n\t'

CHROME_VERSION=${1:-60.0.3112.90-1}
export DEBIAN_FRONTEND=noninteractive

curl -sS -L https://dl.google.com/linux/linux_signing_key.pub | apt-key add -
echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list

apt-get update -q -y

# Download from our local S3 bucket if we can't find the package in the repository
echo "Searching for $CHROME_VERSION in apt repository"
CHECK_VERSION=$(apt-cache show google-chrome-stable | grep Version | grep "$CHROME_VERSION") || true

if [[ -z $CHECK_VERSION ]]; then
    CHROME_DEB="google-chrome-stable_${CHROME_VERSION}_amd64.deb"
    CHROME_URL="https://s3.amazonaws.com/gitlab-google-chrome-stable/${CHROME_DEB}"
    echo "Downloading $CHROME_URL"
    curl --silent --show-error --fail -O $CHROME_URL
    dpkg -i ./$CHROME_DEB || true
    apt-get install -f -y
    rm -f $CHROME_DEB
else
    echo "Installing via apt-get"
    apt-get install -y google-chrome-stable=$CHROME_VERSION
fi

rm -rf /var/lib/apt/lists/*

# Install ChromeDriver
wget -q https://chromedriver.storage.googleapis.com/2.36/chromedriver_linux64.zip
unzip chromedriver_linux64.zip -d /usr/local/bin
rm -f chromedriver_linux64.zip
